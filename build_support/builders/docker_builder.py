#!/usr/bin/env python3
import grp
import os
import subprocess
import sys
from docker_runner import DockerRunner
from options import Options
from project_map import ProjectMap
from utils.command import run_batch_command


class DockerBuilder:
    """ Thin wrapper that uses DockerRunner to run in with CI build.py
    interface """
    def __init__(self, container_tag, run_cmd, env=None, workdir=None,
                 interactive=False, rm=False):
        self._container_tag = container_tag
        self._env = env
        self._run_cmd = run_cmd
        self._workdir = workdir
        self._env = env
        self._interactive = interactive
        self._rm = rm

    def build(self):
        pass

    def test(self):
        drunner = DockerRunner(self._container_tag, self._workdir,
                               interactive=self._interactive)
        drunner.run(self._run_cmd, self._env, rm=self._rm)

    def clean(self):
        pass
