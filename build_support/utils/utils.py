# Copyright (C) Intel Corp.  2014.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Mark Janes <mark.a.janes@intel.com>
#  **********************************************************************/
import base64
import hashlib
import multiprocessing
import os
import sys
import socket
import time
import configparser
import xml.sax.saxutils
from urllib.request import urlopen, Request, URLError, HTTPError

from .command import run_batch_command
from options import Options
from project_map import ProjectMap
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                             "../../..", "mesa_ci_internal"))
try:
    import internal_build_support.vars as internal_vars
except ModuleNotFoundError:
    internal_vars = None


def git_clean(src_dir):
    savedir = os.getcwd()
    os.chdir(src_dir)
    run_batch_command(["git", "clean", "-xfd"])
    run_batch_command(["git", "reset", "--hard", "HEAD"])
    if os.path.exists('.git/rebase-apply'):
        git_env = {"GIT_AUTHOR_NAME" : "Mesa CI",
                   "GIT_AUTHOR_EMAIL" : "mesa_ci@intel.com",
                   "GIT_COMMITTER_NAME" : "Mesa CI",
                   "GIT_COMMITTER_EMAIL" : "mesa_ci@intel.com",
                   "EMAIL" : "mesa_ci@intel.com" }
        run_batch_command(["git", "am", "--abort"], env = git_env)
    os.chdir(savedir)

def cpu_count(options):
    """provide the number of cores on the current machine, with configurable per-label limits"""
    cpus = multiprocessing.cpu_count() + 1
    max_count = {
        "builder" : 25,
        "mtl" : 8,
        "_sim" : cpus // 2,
        }
    for label, count in max_count.items():
        if label in options.hardware:
            cpus = min(cpus, count)
            break
    return cpus

class DefaultTimeout:
    def __init__(self, options=None):
        self._options = options
        if not options:
            self._options = Options()

    def GetDuration(self):
        """by default, components should finish in under 15 minutes.
        For daily builds, 60 minutes is acceptable."""

        if self._options.type == "daily" or self._options.type == "release":
            return 120
        if self._options.hardware == "byt":
            # bay trail takes 15min to rsync to /tmp on the sdcard
            return 30
        return 25


def is_soft_fp64(hardware):
    """ Determine if hardware uses a software fp64 implementation"""
    if not hardware:
        return False
    soft_fp64_hw = {'icl', 'tgl', 'dg2'}
    if internal_vars:
        soft_fp64_hw.update(internal_vars.soft_fp64_hw)
    for soft in soft_fp64_hw:
        # check for substrings, eg icl is in icl_zink
        if soft in hardware:
            return True
    return hardware in soft_fp64_hw


class NoConfigFile(Exception):
    def __str__(self):
        return ("ERROR: No conf file found. Please create a conf for this "
                "hardware platform under <mesa_jenkins>/<test>/<hardware>."
                "conf")


def get_conf_file(hardware, arch, project="piglit-test", _base_dir=None):
    pm = ProjectMap()
    base_dir = _base_dir
    if not base_dir:
        base_dir = pm.source_root()
    conf_dir = base_dir + "/" + project + "/"
    conf_file = conf_dir + "/" + hardware + arch + ".conf"
    if not os.path.exists(conf_file):
        conf_file = conf_dir + "/" + hardware + ".conf"

    if os.path.exists(conf_file):
        return conf_file

    if "gt" in hardware:
        # we haven't found a sku-specific conf, so strip the gtX
        # strings off of thef hardware and try again.
        return get_conf_file(hardware[:3], arch, project, _base_dir)

    if _base_dir:
        raise NoConfigFile
    # Try again but use internal conf path
    return get_conf_file(hardware, arch, project,
                         base_dir + "/repos/mesa_ci_internal/" + "/")


def mesa_version(env=None):
    br = ProjectMap().build_root()
    wflinfo = br + "/bin/wflinfo"
    if not env:
        env = {}
    env.update({
        'LD_LIBRARY_PATH': ':'.join([
            get_libdir(),
            get_libgl_drivers(),
            os.path.join(br, 'lib', 'piglit', 'lib'),
        ]),
        "LIBGL_DRIVERS_PATH": get_libgl_drivers(),
    })
    (out, _) = run_batch_command([wflinfo,
                                 "--platform=gbm", "-a", "gl"],
                                 streamedOutput=False, env=env)
    for a_line in out.splitlines():
        a_line = a_line.decode()
        if "OpenGL version string" not in a_line:
            continue
        tokens = a_line.split(":")
        assert len(tokens) == 2
        version_string = tokens[1].strip()
        version_tokens = version_string.split()
        assert len(version_tokens) >= 3
        for token in version_tokens:
            if '.' in token:
                # token is a version string, eg "20.1"
                return token
        assert False


def _system_dirs():
    """Returns the correct lib prefix for debian vs non-debian."""
    lib_dirs = []
    if Options().arch == "m32":
        lib_dirs.append("lib32")
        lib_dirs.append("lib/i386-linux-gnu")
    else:
        lib_dirs.append("lib64")
        lib_dirs.append("lib/x86_64-linux-gnu")
    # add 'lib' here since it can cause conflicts on some distros (Arch)
    # where */lib contains m64 binaries
    lib_dirs.append('lib')
    return lib_dirs


def get_package_config_path():
    lib_dirs = _system_dirs()
    build_root = ProjectMap().build_root()
    pkg_configs = ([os.path.join(build_root, l, 'pkgconfig') for l in lib_dirs]
                   + [os.path.join('/usr', l, 'pkgconfig') for l in lib_dirs])
    # meson has deprecated support for specifying paths in pkg_config_path more
    # than once, so remove any duplicates while maintaining the same ordering
    pkg_configs = list(dict.fromkeys(pkg_configs))
    return ':'.join(pkg_configs)


def get_libgl_drivers():
    """Get the correct drivers dir depending on platform and arch."""
    lib_dirs = _system_dirs()
    build_root = ProjectMap().build_root()
    return ':'.join(
        [os.path.join(build_root, l, 'dri') for l in lib_dirs] +
        [os.path.join('/usr', l, 'dri') for l in lib_dirs])


def get_libdir():
    """Get the correct libdir depending on platform and arch."""
    lib_dirs = _system_dirs()
    build_root = ProjectMap().build_root()
    return ':'.join(
        [os.path.join(build_root, l) for l in lib_dirs] +
        [os.path.join('/usr', l) for l in lib_dirs])


class NullInvoke:
    """masquerades as an invoke object, so the main routine can post
    results even if there is no server to post to"""
    def __init__(self):
        pass

    def set_info(self, *args):
        pass

    def set_status(self, *args):
        pass


def write_pid(pidfile):
    """Write the PID file."""
    with open(pidfile, 'w') as f:
        f.write(str(os.getpid()))


class TimeoutException(Exception):
    def __init__(self, msg):
        Exception.__init__(self)
        self._msg = msg

    def __str__(self):
        return self._msg


def signal_handler(signum, frame):
    raise TimeoutException("Fetch timed out.")


def signal_handler_quit(signum, frame):
    sys.exit(-1)


def file_checksum(fname):
    """ Generate md5 checksum for given file """
    with open(fname, 'rb') as f:
        return hashlib.md5(f.read()).hexdigest()

def reliable_url_open(url, method=None):
    # We use a loop to open the url because of DE3123
    failcount = 0
    request = Request(url, method=method)

    if "JENKINS_USER" in os.environ and "JENKINS_PW" in os.environ:
            user = os.getenv("JENKINS_USER").strip()
            token = os.getenv("JENKINS_PW").strip()
            base64string = base64.b64encode(bytes(f'{user}:{token}','ascii'))

            request.add_header("Authorization", f"Basic {base64string.decode('utf-8')}")

    while True:
        try:
            f = urlopen(request)
            return f
        except (HTTPError, URLError):
            print(f"failure urllib2.urlopen(\"{url}\")")
            failcount += 1
            if failcount == 5:
                raise
            time.sleep(5)

def get_blacklists(project_map=None, opts=None):
    """
    Collect list of blacklist files for given project (e.g. 'glcts-test')
    """
    if not project_map:
        project_map = ProjectMap()
    if not opts:
        opts = Options()
    bdir = project_map.project_build_dir()
    project = project_map.current_project()
    hw_prefix = opts.hardware[:3]
    possible_blacklists = [bdir + opts.hardware + "_blacklist.conf",
                           bdir + hw_prefix + "_blacklist.conf",
                           bdir + opts.arch + "_blacklist.conf",
                           bdir + hw_prefix + '_' + opts.arch + "_blacklist.conf",
                           bdir + "blacklist.conf",
                           project_map.source_root() + ("/repos/mesa_ci_internal/"
                                               + project
                                               + "/blacklist.conf"),
                           project_map.source_root() + ("/repos/mesa_ci_internal/"
                                               + project + '/'
                                               + opts.hardware
                                               + "_blacklist.conf")
                           ]
    if is_soft_fp64(opts.hardware) and opts.type != 'daily':
        possible_blacklists.append(bdir + 'soft-fp64_blacklist.conf')
    if opts.type != "daily" and not opts.retest_path:
        possible_blacklists.append(bdir + 'non-daily_blacklist.conf')
    if "zink" in opts.hardware:
        possible_blacklists.append(bdir + 'zink_blacklist.conf')
    if opts.hardware.endswith('_sim'):
        possible_blacklists += [
            bdir + 'sim_blacklist.conf',
            project_map.source_root() + ("/repos/mesa_ci_internal/"
                                + project
                                + "/sim_blacklist.conf")
        ]

    blacklist_files = []
    for file in possible_blacklists:
        if os.path.exists(file) and file not in blacklist_files:
            blacklist_files.append(file)
    return blacklist_files

class Flake:
    """Encapsulates handling of the repeated execution of a flaky test"""
    def __init__(self, expected):
        self._expected = expected
        self._results = []
        self._test_outputs = []

    def add_result(self, status, test_output):
        """Track the current iteration of this test, for subsequent summary"""
        self._test_outputs.append(test_output)
        self._results.append(status)

    def to_xml(self, fh, name):
        """Summarize the results of the current test's repeated execution"""
        match_count = 0
        full_output = ""
        default_expected = self._expected
        if default_expected == "random":
            default_expected = "pass"
        for index in range(len(self._results)):
            if self._results[index] == default_expected:
                match_count += 1
            full_output += self._test_outputs[index] + "\n"
        miss_count = len(self._results) - match_count
        ending = "/>"
        if full_output:
            ending = ">"
        fh.write(f'  <test name="{name}"\n'
                 f'        expected="{self._expected}"\n'
                 f'        match="{str(match_count)}"\n'
                 f'        miss="{str(miss_count)}"{ending}\n')
        if full_output:
            fh.write("    <output>\n")
            fh.write(xml.sax.saxutils.escape(full_output))
            fh.write("    </output>\n")
            fh.write("  </test>\n")

# needed to preserve case in the options
class CaseConfig(configparser.SafeConfigParser):
    """For handling config files with flaky test names"""
    def optionxform(self, optionstr):
        return optionstr

class Flakes:
    """Encapsulates logic for reporting execution of flaky tests"""
    def __init__(self, project, opts):
        self._project = project
        self._opts = opts
        self._flakes = {}
        p_map = ProjectMap()
        build_dir = p_map.project_build_dir(project=project)
        int_build_dir = p_map.internal_project_build_dir(project=project)
        possible_flake_files = [f"{build_dir}/flaky_{self._opts.hardware}.conf",
                                f"{build_dir}/flaky.conf",
                                f"{build_dir}/flaky_{self._opts.arch}.conf",
                                f"{int_build_dir}/flaky_{self._opts.hardware}.conf",
                                f"{int_build_dir}/flaky.conf",
                                f"{int_build_dir}/flaky_{self._opts.arch}.conf",]
        if "zink" in self._opts.hardware:
            possible_flake_files.append(f"{build_dir}/flaky_zink.conf")
        for _f in possible_flake_files:
            if not os.path.exists(_f):
                continue
            with open(_f, "r", encoding='utf8') as _fh:
                p = CaseConfig(allow_no_value=True)
                p.optionxform = str
                p.readfp(_fh)
                assert p.has_section("expected-random")
                assert p.has_section("expected-fail")
                assert p.has_section("expected-pass")
                for test, _ in p.items("expected-random"):
                    if test in self._flakes:
                        print(f"WARNING: flaky {test} listed more than once: {_f}")
                        continue
                    self._flakes[test] = Flake("random")
                for test, _ in p.items("expected-pass"):
                    if test in self._flakes:
                        print(f"WARNING: flaky {test} listed more than once: {_f}")
                        continue
                    self._flakes[test] = Flake("pass")
                for test, _ in p.items("expected-fail"):
                    if test in self._flakes:
                        print(f"WARNING: flaky {test} listed more than once: {_f}")
                        continue
                    self._flakes[test] = Flake("fail")

    def is_flake_shard(self):
        return self._opts.shard == "0" or self._opts.shard.startswith("1:")

    def test_list(self):
        """"Generate a list of tests to be executed"""
        if self._opts.retest_path != "":
            return []
        return list(self._flakes.keys())

    def expand_test_group(self, group_name, tests):
        """"replaces a flaky test with a list of flaky tests"""
        flake = self._flakes[group_name]
        for test in tests:
            self._flakes[test] = Flake(flake._expected)
        if group_name not in tests:
            del(self._flakes[group_name])

    def add_result(self, test_name, status, test_output):
        """Include the current iteration of this test result in the flaky output"""
        if test_name not in self._flakes:
            print(f"WARN: extra result: {test_name}")
            return
        self._flakes[test_name].add_result(status, test_output)

    def deny_test(self, denied):
        if not denied:
            return
        if denied.startswith("#"):
            return
        for key in list(self._flakes):
            if denied in key:
                del self._flakes[key]

    def generate_xml(self):
        """Summarize the flaky results into an xml file for this project/platform"""
        if not self.is_flake_shard():
            return
        tdir = ProjectMap.test_dir(ProjectMap)
        out_path =  f"{tdir}/flaky_{self._project}_{self._opts.hardware}_{self._opts.arch}_{self._opts.shard}.xml"
        with open(out_path, "w", encoding='utf8') as _fh:
            _fh.write("<?xml version='1.0' encoding='utf-8'?>\n")
            _fh.write(f'<FlakyTests project="{self._project}"\n'
                      f'            hardware="{self._opts.hardware}"\n'
                      f'            arch="{self._opts.arch}">\n')
            for name, flake in self._flakes.items():
                flake.to_xml(_fh, name)
            _fh.write("</FlakyTests>\n")

def write_disabled_tests(project_map, opts, test_trie, suffix=""):
    """write the canonical file listing of all disabled tests for the
    current project.  Suffix parameter is used when multiple suite
    listers are needed within a suite (for multiple binaries)
    """
    if opts.shard != '0' and not opts.shard.startswith("1:"):
        # only write the file for the first shard
        return
    # write file with denylisted tests
    disabled_tests = test_trie.matching_tests_for_files(get_blacklists())
    tdir = project_map.test_dir()
    out_path = (f"{tdir}/disabled_{project_map.current_project()}_"
                f"{opts.hardware}_{opts.arch}{suffix}.txt")
    with open(out_path, "w", encoding="utf8") as outf:
        outf.write("<?xml version='1.0' encoding='utf-8'?>\n")
        outf.write(f'<DisabledTests project="{project_map.current_project()}" '
                   f'hardware="{opts.hardware}" '
                   f'arch="{opts.arch}">\n')
        for disabled in disabled_tests:
            outf.write(f'  <test name="{disabled}"/>\n')
        outf.write('</DisabledTests>')
