# Copyright (C) Intel Corp.  2022.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Mark Janes <markjanes@swizzler.org>
#  **********************************************************************/

import os, sys
import xml.etree.cElementTree as et

class BuildSpec:
    """Convenience methods for accessing configuration in the buildspec xml"""
    def __init__(self, xml):
        self._xml = xml

    def _string_spec(self, spec, indent=0):
        indent_str = " " * indent
        ret_string = indent_str + spec.tag
        for key, value in spec.attrib.items():
            ret_string += " " + key + "=" + value
        ret_string += "\n"
        for subtag in spec:
            ret_string += self._string_spec(subtag, indent +1)
        return ret_string

    def string_spec(self):
        """Write the specification as an string"""
        root = self._xml.getroot()
        return self._string_spec(root)

    def default_branch_for_repo(self, repo):
        """return the git branch that will be selected by default for the given repo"""
        repo_tag = self._xml.find(f"repos/{repo}")
        if "branch" in repo_tag.attrib:
            return repo_tag.attrib["branch"]
        return "origin/master"

    def default_pipeline_for_ci_branch(self, branch_name):
        """look up the pipeline for which the ci branch is nested under in jenkins, eg:
        gl_main -> external_pipeline"""
        for branch_tag in self._xml.find("branches"):
            if branch_name == branch_tag.attrib["name"]:
                if "pipeline" in branch_tag.attrib:
                    return branch_tag.attrib["pipeline"]
        return ''

    def default_project_for_ci_branch(self, branch_name):
        """look up the project that would get build for the ci branch, eg:
        mesa_master -> all-test"""
        for branch_tag in self._xml.find("branches"):
            if branch_name == branch_tag.attrib["name"]:
                return branch_tag.attrib["project"]
        assert False

    def priority_for_ci_branch(self, branch):
        """Look up the priority of the branch, defaulting to 3"""
        # priority from env should take precedence
        if 'BUILD_PRIORITY' in os.environ:
            try:
                return int(os.environ.get('BUILD_PRIORITY'))
            except TypeError:
                pass
        for project_tag in self._xml.findall("branches/branch"):
            if branch == project_tag.attrib["name"]:
                if "priority" in project_tag.attrib:
                    return int(project_tag.attrib["priority"])
        return 3

    def default_branches_for_ci_branch(self, branch_name):
        """return the git branches that should get checked out for the ci
        branch if no other information is specified"""
        default_branches = {}
        branch_tag = None
        for tag in self._xml.findall("branches/branch"):
            if tag.attrib["name"] == branch_name:
                branch_tag = tag
        if branch_tag is None:
            print(f"Error: invalid branch - {branch_tag}")
            sys.exit(-1)
        for repo_tag in branch_tag:
            repo_name = repo_tag.tag
            if "branch" in repo_tag.attrib:
                default_branches[repo_name] = repo_tag.attrib["branch"]
            else:
                default_branches[repo_name] = self.default_branch_for_repo(repo_name)
        return default_branches

    def repo_name_for_project(self, project):
        """identify the repo where the project is checked out, if any"""
        # most projects have an identically named repo
        repo = project
        if self._xml.find(f"repos/{repo}") is None:
            # check to see if the project has a custom src_dir
            for project_tag in self._xml.findall("projects/project"):
                if project_tag.attrib["name"] == project:
                    if "src_dir" in project_tag.attrib:
                        repo = project_tag.attrib["src_dir"]
                    break

        if self._xml.find(f"repos/{repo}") is None:
            # this project has no source dir
            return None
        return repo

    def master_host(self):
        """get the host name of the ci master"""
        return self._xml.find("build_master").attrib["host"]

    def master_hostname(self):
        """get the host name of the ci master"""
        return self._xml.find("build_master").attrib["host"].split(".")[0]

    def git_host(self):
        """get the fqdn for the git server"""
        return self._xml.find("git_server").attrib["host"]

    def git_hostname(self):
        """get the hostname for the git server"""
        return self._xml.find("git_server").attrib["host"].split(".")[0]

    def results_host(self):
        """get the fqdn for the results server"""
        return self._xml.find("results_server").attrib["host"]

    def results_hostname(self):
        """get the hostname for the results server"""
        return self._xml.find("results_server").attrib["host"].split(".")[0]

    def xml(self):
        """Provide the etree for the caller to peruse"""
        return self._xml

    def watched_branches_in_remote(self, repo_name, remote_name):
        """generate a list of the git branches that trigger jobs in CI, on the 
        specified repo/remote"""
        watched = []
        for repo_tag in self._xml.findall(f"branches/branch/{repo_name}"):
            if "branch" not in repo_tag.attrib:
                continue
            branch_name = repo_tag.attrib["branch"]
            if remote_name != branch_name.split("/")[0]:
                continue
            if "trigger" in repo_tag.attrib and repo_tag.attrib["trigger"] == "False":
                continue
            watched.append("/".join(branch_name.split("/")[1:]))
        branch_name = self.default_branch_for_repo(repo_name)
        if remote_name == branch_name.split("/")[0]:
            watched.append("/".join(branch_name.split("/")[1:]))
        return watched
