#!/usr/bin/env python3

# Copyright (C) Intel Corp.  2014.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Yuriy Bogdanov <yuriy.bogdanov@intel.com>


import argparse
import ast
import os
import requests
import sys
import time

try:
    from urllib2 import urlopen, urlencode, URLError, HTTPError, quote
except:
    from urllib.request import urlopen, URLError, HTTPError, quote
    from urllib.parse import urlencode

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "build_support"))
from utils.utils import reliable_url_open

authorized_keys = '/home/jenkins/.ssh/authorized_keys'
users = [
    'aswarup',
    'cmarcelo',
    'currojerez',
    'dbaker',
    'fjdegroo',
    'idr',
    'jljusten',
    'jxzgithub',
    'kwg',
    'llandwerlin',
    'majanes',
    'mslusarz',
    'nchery',
    'ngcortes',
    'sagarghuge',
    'shadeslayer',
    'tpalli',
    'ybogdano',
    'zehortigoza'
]
jenkins_ssh_key = ('ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCiFl/afTIOYeDXoom0tg0T4EmpQSOCdX1Jn3/Jh3n'
                   'FIByfwTzlhzsBI0YFo3eAXxSwfv1IOgH3RA92K0dfEZooJwlxYMLGQ2v+XbwwzlsEOUscUEGtX+D5vM'
                   'iIebHievRL8nzAUtLj8L2gQf5WtDwN+wdGh3XriSFVzLiZlWBiBw+97TMh18CqemqbfmZ7BZnRQAXx+'
                   'hj8QtkQrPaiu7RKCbv2v1Nf3lDhR7vRSEQh3xT0RCoaLYyhXSu1brosUPlo/FyuM2ZWbpthPBpfHLkT'
                   'LI88L/q2K/zfdpMfGI8O0kLt4aPUkvU+MMmfSB19uNEGsMJ3fRd6PzE3Nkzle9tn '
                   'jenkins@otc-mesa-ci')

def is_excluded(a_host):
    """don't send a build to the master system, which should not automatically deply ssh key"""
    if a_host == "master":
        return True
    if a_host == "Built-In Node":
        return True
    return False


def generate_authorized_keys_file(key):
    try:
        with open(authorized_keys, 'r', encoding="utf-8") as file:
            content = file.read()
        if key not in content:
            with open(authorized_keys, 'a', encoding="utf-8") as file:
                file.write(key)
    except:
        if key not in authorized_keys:
            with open(authorized_keys, 'a', encoding="utf-8") as file:
                file.write(f'{key}')


def get_users(users=users):
    for user in users:

        url = requests.get(f'https://gitlab.freedesktop.org/{user}.keys').text

        if '!DOCTYPE html' not in url:
            ssh_key_data =  list(filter(None, url.split('\n'))) # clear all new lines from input
            for ssh_key in ssh_key_data:
                key = ' '.join(ssh_key.split(' ')[:-1])
                generate_authorized_keys_file(f'{key}\n')
        else:
            print(f"Could not get key for {user}!")
    generate_authorized_keys_file(f'{jenkins_ssh_key}\n')


def trigger_builds(jenkins_label, build_support_branch="origin/master", local=False):
    """On any system, determine which test machines are online and
    send a build to them"""
    server = "mesa-ci-jenkins.jf.intel.com"
    url = "http://" + server + "/computer/api/python"
    node_name = os.getenv("NODE_NAME")

    if (node_name == jenkins_label) or local:
        get_users()
        sys.exit(0)

    host_dict = ast.literal_eval(reliable_url_open(url, method="POST").read().decode('utf-8'))
    for a_host in host_dict['computer']:
        offline = a_host['offline']
        host = a_host['displayName']
        label_list = list(map(lambda i: i["name"], a_host["assignedLabels"]))

        if is_excluded(host):
            continue
        elif jenkins_label in label_list:
            options = {'build_support_branch': build_support_branch,
                       'label': host}
            url = f"http://{server}/job/public/job/deploy_ssh/buildWithParameters?{urlencode(options)}"
            print("triggering " + url)
            reliable_url_open(url, method="POST")
            time.sleep(1)


if __name__ == '__main__':
    if not os.path.exists('/home/jenkins/.ssh'):
        os.makedirs('/home/jenkins/.ssh')
    argparser = argparse.ArgumentParser(description=
                                        "Deploy ssh authorized keys on curent machene",
                                        formatter_class=argparse.RawTextHelpFormatter)
    argparser.add_argument("jenkins_label", help="can be a jenkins machine name or a label (eg. bdwgt3-01 or bdw)")
    argparser.add_argument("--build_support_branch", help="points to a sha or branch of mesa_jenkins to checkout")
    argparser.add_argument("--local", action="store_true", help="Deploy ssh authorized keys on curent machene")
    args = argparser.parse_args()

    trigger_builds(args.jenkins_label, build_support_branch=args.build_support_branch, local=args.local)
