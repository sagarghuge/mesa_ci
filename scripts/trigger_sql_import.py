import sys
import os
import json

from urllib.parse import urlparse, urlencode
from urllib.request import quote

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "build_support"))

from utils.utils import reliable_url_open

url_components = urlparse(os.getenv("BUILD_URL"))
result_path = None
export_params = None

with open(os.path.join(os.getenv('WORKSPACE'), "build_info.json"), "r") as fh:
    build_info = json.load(fh)
    export_params = urlencode({
        "result_path": quote(build_info["result_path"]),
        "mesa_ci_branch": build_info["revisions"]["mesa_ci"]["sha"]
    })

export_url = (f"http://{url_components.netloc}"
              "/job/public/job/export_public_results/buildWithParameters?"
              f"{export_params}")

print("triggering: " + export_url)
reliable_url_open(export_url, method="POST")
